using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscController : MonoBehaviour {

	public OSC osc;
	private ControllerScript cScript;


	// Use this for initialization
	void Start () {

		osc.SetAddressHandler("/ResetScene", ResetScene);
		osc.SetAddressHandler("/StartNewTrial", StartNewTrial);
		osc.SetAddressHandler("/SetAcousticTalker", SetAcousticTalker);


		osc.SetAllMessageHandler (onAllOscMessages);


		cScript = transform.GetComponent<ControllerScript> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ResetScene(OscMessage message) {

		cScript.ResetTalkers();

		Debug.Log ("OSC receive: " + message);
	}

	void StartNewTrial(OscMessage message) {

        cScript.trialActiveTime = message.GetFloat(0);
        cScript.StartNewTrial();


		Debug.Log ("OSC receive: " + message);
	}
		
	void SetAcousticTalker(OscMessage message) {

		int tIdx = 1;
		for(int i = 0; i < cScript.talkerArr.GetLength(0); i++){
			for (int j = 0; j < cScript.talkerArr.GetLength (1); j++) {

				if (tIdx == System.Convert.ToInt32(message.values[0])) {

					cScript.talkerArr [i, j].transform.GetComponent<TalkerController> ().acousticCharacter = System.Convert.ToInt32(message.values [1]);
				}

				tIdx++;
			}
		}

		cScript.ShowAcousticTalker();


		Debug.Log ("OSC receive: " + message);
	}
				
	void onAllOscMessages(OscMessage message){ // All OSC messages independent of adress are handled by this function 

		int i = 0;
		while (i < message.values.Count) {

			Debug.Log ("OSC log: " + message.ToString());

			i++;
		}
	}
}

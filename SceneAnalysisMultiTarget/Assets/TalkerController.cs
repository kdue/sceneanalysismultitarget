﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkerController : MonoBehaviour {

	public int character = -1;
	public int acousticCharacter = -1;

	public GameObject AcousticTalkerIndicatorPF;
	private GameObject AcousticTalkerIndicator;

	private Animator anim;
	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();

		AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
		anim.Play(state.fullPathHash, -1, Random.Range(0f, 1f)); // start animation at random frame

		anim.speed = Random.Range (0.7f, 1.2f);

		AcousticTalkerIndicator = Instantiate (AcousticTalkerIndicatorPF,transform);
		AcousticTalkerIndicator.name = "AcousticTalkerIndicator";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;
using System.Collections;

namespace Valve.VR.Extras
{
    public class SteamVR_LaserPointer : MonoBehaviour
    {
        public SteamVR_Behaviour_Pose pose;

        //public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.__actions_default_in_InteractUI;
        public SteamVR_Action_Boolean triggerClick = SteamVR_Input.GetBooleanAction("GrabPinch");

        public bool active = true;
        public Color color;
        public float thickness = 0.002f;
        public Color clickColor = Color.green;
        public GameObject holder;
        public GameObject pointer;
        bool isActive = false;
        public bool addRigidBody = false;
        public Transform reference;
        public event PointerEventHandler PointerIn;
        public event PointerEventHandler PointerOut;
        public event PointerEventHandler PointerClick;

        Transform previousContact = null;

        //KASPER
        public RaycastHit currHit;
        public RaycastHit laserHit;
        public RaycastHit updatedLaserHitPos;
        public bool hitDetected = false;

        private float laserElevOffset, laserPosOffsetX, laserPosOffsetY, laserPosOffsetZ;

		public int colorIdx = 0;

        private void Start()
        {
            if (pose == null)
                pose = this.GetComponent<SteamVR_Behaviour_Pose>();
            if (pose == null)
                Debug.LogError("No SteamVR_Behaviour_Pose component found on this object");
            
            if (triggerClick == null)
                Debug.LogError("No trigger interaction action has been set on this component.");
            

            holder = new GameObject();
            holder.transform.parent = this.transform;
            holder.transform.localPosition = Vector3.zero;

            if (UnityEngine.XR.XRDevice.model.Contains("Oculus")){

                //stretched arm - oculus
                //laserElevOffset = -1.5f;
                //laserPosOffsetX = 0.0f;
                //laserPosOffsetY = -0.01f;
                //laserPosOffsetZ = -0.03f;

                //gun pose - oculus 
                laserElevOffset = 39.3f;
                laserPosOffsetX = 0.03f;
                laserPosOffsetY = -0.025f;
                laserPosOffsetZ = -0.03f;
            }
            else
            {

                //stretched arm - vive
                laserElevOffset = -1.28f;
                laserPosOffsetX = 0.0f;
                laserPosOffsetY = -0.01f;
                laserPosOffsetZ = -0.00f;

                //gun pose - vive
                //laserElevOffset = 57.889f;
                //laserPosOffsetX = 0.0f;
                //laserPosOffsetY = 0.004f;
                //laserPosOffsetZ = 0.008f;
            }

            //KASPER
            holder.transform.localPosition = new Vector3(laserPosOffsetX, laserPosOffsetY, laserPosOffsetZ);


            holder.transform.localRotation = Quaternion.identity;
      
            //KASPER
            Quaternion tmpRotOffset = holder.transform.localRotation;
            tmpRotOffset.eulerAngles = new Vector3(laserElevOffset, 0.0f, 0.0f);
            holder.transform.localRotation = tmpRotOffset;


            pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
            pointer.transform.parent = holder.transform;
            pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
            pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
            pointer.transform.localRotation = Quaternion.identity;
            BoxCollider collider = pointer.GetComponent<BoxCollider>();
            if (addRigidBody)
            {
                if (collider)
                {
                    collider.isTrigger = true;
                }
                Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
            else
            {
                if (collider)
                {
                    Object.Destroy(collider);
                }
            }
            Material newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor("_Color", color);
            pointer.GetComponent<MeshRenderer>().material = newMaterial;


            //KASPER
            currHit = new RaycastHit();
        }

        public virtual void OnPointerIn(PointerEventArgs e)
        {
            if (PointerIn != null)
                PointerIn(this, e);
        }

        public virtual void OnPointerClick(PointerEventArgs e)
        {
            if (PointerClick != null)
                PointerClick(this, e);
        }

        public virtual void OnPointerOut(PointerEventArgs e)
        {
            if (PointerOut != null)
                PointerOut(this, e);
        }

        
        private void Update()
        {
            if (!isActive)
            {
                isActive = true;
                this.transform.GetChild(0).gameObject.SetActive(true);
            }

            float dist = 100f;

            //Ray raycast = new Ray(transform.position, transform.forward);
            //KASPER
            Ray raycast = new Ray(holder.transform.position, holder.transform.forward);
      
            RaycastHit hit;
            bool bHit = Physics.Raycast(raycast, out hit);

            if (previousContact && previousContact != hit.transform)
            {
                PointerEventArgs args = new PointerEventArgs();
                args.fromInputSource = pose.inputSource;
                args.distance = 0f;
                args.flags = 0;
                args.target = previousContact;
                OnPointerOut(args);
                previousContact = null;
            }
            if (bHit && previousContact != hit.transform)
            {
                PointerEventArgs argsIn = new PointerEventArgs();
                argsIn.fromInputSource = pose.inputSource;
                argsIn.distance = hit.distance;
                argsIn.flags = 0;
                argsIn.target = hit.transform;
                OnPointerIn(argsIn);
                previousContact = hit.transform;

                //KASPER
                hitDetected = Physics.Raycast(holder.transform.position, holder.transform.TransformDirection(Vector3.forward), out hit, 100.0f);
                if (hitDetected == true)
                {
                    currHit = hit;
                }
            }


            if (!bHit)
            {
                previousContact = null;
            }
            if (bHit && hit.distance < 100f)
            {
                dist = hit.distance;

                //KASPER
                updatedLaserHitPos = hit;
            }

            if (bHit && triggerClick.GetStateUp(pose.inputSource))
            {
                PointerEventArgs argsClick = new PointerEventArgs();
                argsClick.fromInputSource = pose.inputSource;
                argsClick.distance = hit.distance;
                argsClick.flags = 0;
                argsClick.target = hit.transform;
                OnPointerClick(argsClick);
            }

            if (triggerClick != null && triggerClick.GetState(pose.inputSource))
            {
                pointer.transform.localScale = new Vector3(thickness * 2f, thickness * 2f, dist);
                //pointer.GetComponent<MeshRenderer>().material.color = clickColor;
                
                //KASPER
                laserHit = hit;
            }
            else
            {
                pointer.transform.localScale = new Vector3(thickness, thickness, dist);
                pointer.GetComponent<MeshRenderer>().material.color = color;
            }
            pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
        }
    }

    public struct PointerEventArgs
    {
        public SteamVR_Input_Sources fromInputSource;
        public uint flags;
        public float distance;
        public Transform target;
    }

    public delegate void PointerEventHandler(object sender, PointerEventArgs e);
}
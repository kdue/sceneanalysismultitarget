﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkerColors : MonoBehaviour
{

	public Color blackOutline;

	public float defaultJointAlpha = 0.1f;
	public float defaultSurfAlpha = 0.1f;
	public float selectedJointAlpha = 0.4f;
	public float selectedSurfAlpha = 0.4f;

	public float defaultJointOutlineWidth = 0.05f;
	public float defaultSurfOutlineWidth = 0.15f;
	public float selectedJointOutlineWidth = 0.2f;
	public float selectedSurfOutlineWidth = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
		blackOutline = new Color (0.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Color CharToColor(int charNum)
    {
        Color col = new Color();

        switch (charNum)
        {
			case -1:
				col = new Color(128.0f, 128.0f, 128.0f);
				break;
            case 0:
                col = new Color(255.0f, 0.0f, 0.0f);
                break;
            case 1:
				col = new Color(245.0f, 130.0f, 48.0f);
                break;
            case 2:
				col = new Color(255.0f, 255.0f, 25.0f);
                break;
            case 3:
				col = new Color(0.0f, 200.0f, 0.0f);
                break;
            case 4:
                col = new Color(70.0f, 240.0f, 240.0f);
                break;
            case 5:
				col = new Color(0.0f, 130.0f, 200.0f);
                break;
            case 6:
				col = new Color(145.0f, 30.0f, 180.0f);
			    break;
			case 7:
				col = new Color(240.0f, 50.0f, 230.0f);
				break;
			case 8:
				col = new Color(0.0f, 0.0f, 0.0f);
				break;
			case 9:
				col = new Color(255.0f, 255.0f, 255.0f);
				break;
    
        }

        return col / 256.0f;
    }

    public void SetColor(Material jointMat, Material surfMat, Color bodyColor, Color outlineColor, float bodyAlpha, float surfAlpha)
    {
        jointMat.SetColor("_BodyColor", bodyColor); 
        surfMat.SetColor("_BodyColor", bodyColor);
        jointMat.SetColor("_OutlineColor", outlineColor);
        surfMat.SetColor("_OutlineColor", outlineColor);
        jointMat.SetFloat("_BodyAlpha", bodyAlpha);
        surfMat.SetFloat("_BodyAlpha", surfAlpha);

    }
}

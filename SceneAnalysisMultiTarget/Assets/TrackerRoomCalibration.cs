﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class TrackerRoomCalibration : MonoBehaviour {

    public GameObject tracker1;
    public GameObject tracker2;
    public GameObject tracker3;

    public GameObject trackerRef1;
    public GameObject trackerRef2;
    public GameObject trackerRef3;

    private Vector3 trackerNorm;
    private Vector3 refNorm;


    private GameObject tmpRotParent;

    private bool isCalibrated;

    private Vector3 tracker2CalPos;
    private Vector3 tracker3CalPos;

    private readonly float trackerAllowedError = 0.01f; // Allowed offset between reference and tracker in meters


    // Use this for initialization
    void Start()
    {

        isCalibrated = false;
       

    }

    // Update is called once per frame
    void Update()
    {

        if (tracker1.activeSelf && tracker2.activeSelf && tracker3.activeSelf)
        { // All three trackers needs to be active to perform calibration
     
            bool trackerPosDiffBool = (Vector3.Distance(tracker1.transform.position, trackerRef1.transform.position) > trackerAllowedError ||
                                        Vector3.Distance(tracker2.transform.position, tracker2CalPos) > trackerAllowedError ||
                                        Vector3.Distance(tracker3.transform.position, tracker3CalPos) > trackerAllowedError); // difference between physical and virtual pos of trackers

            if (SteamVR.outOfRange)
            { // if tracking of HMD is lost

                Debug.Log ("Lost tracking!");

                isCalibrated = false;
            }
            else if (!isCalibrated || trackerPosDiffBool || Input.GetKeyUp("z"))
            { // recalibrate after tracking is lost


                // Fix tracker indeces according to serial number
                SetCorrectTrackerIndex();


                //Create temporary rotation object at local zero with zero initial rotation
                tmpRotParent = new GameObject("tmpRotParent");
                tmpRotParent.transform.SetParent(transform);
                tmpRotParent.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

                for (int i = 0; i < transform.childCount; i++)
                { //Make all children of the rotation object children of the tmpRotParent

                    if (!string.Equals(transform.GetChild(i).name, tmpRotParent.transform.name))
                    {

                        transform.GetChild(i).SetParent(tmpRotParent.transform);
                        i--; //account for the gameobject that is just moved
                    }
                }

                //Normal for physical trackers
                trackerNorm = Vector3.Cross(tracker2.transform.position - tracker1.transform.position, tracker3.transform.position - tracker1.transform.position);

                //Normal for references
                refNorm = Vector3.Cross(trackerRef2.transform.position - trackerRef1.transform.position, trackerRef3.transform.position - trackerRef1.transform.position);

                //Find rotation of tracker space to match normal of reference space 
                Quaternion normRot = Quaternion.FromToRotation(trackerNorm, refNorm);

                //Apply rotation to tmpRotParent
                tmpRotParent.transform.rotation = normRot;

                ChangePos();


                // Y-angle difference between tracker 1 and 2 and ref 1 and 2
                float refAngXZ = Mathf.Atan2(trackerRef2.transform.position.x - trackerRef1.transform.position.x, trackerRef2.transform.position.z - trackerRef1.transform.position.z) * Mathf.Rad2Deg;
                float trackAngXZ = Mathf.Atan2(tracker2.transform.position.x - tracker1.transform.position.x, tracker2.transform.position.z - tracker1.transform.position.z) * Mathf.Rad2Deg;
                float calAngXZ = refAngXZ - trackAngXZ;

                //Rotate angle difference
                tmpRotParent.transform.Rotate(0.0f, calAngXZ, 0.0f, Space.World);

                ChangePos();

                for (int i = 0; i < tmpRotParent.transform.childCount; i++)
                { //Make all children of tmpRotParent children of the rotation object

                    tmpRotParent.transform.GetChild(i).SetParent(transform);
                    i--; //account for the gameobject that is just moved
                }

                GameObject.Destroy(tmpRotParent); //destroy tmpRotParent object

                tracker2CalPos = tracker2.transform.position; // save calibrated tracker2 and tracker3 positions
                tracker3CalPos = tracker3.transform.position;

                isCalibrated = true;

                Debug.Log("Calibrated!");
            }

        }
    }

    private void ChangePos()
    {

        // Difference in position of tracker1 and ref1
        Vector3 trackerPosOffset = new Vector3(tracker1.transform.position.x - trackerRef1.transform.position.x,
            tracker1.transform.position.y - trackerRef1.transform.position.y,
            tracker1.transform.position.z - trackerRef1.transform.position.z);

        //Translate tracker space according to position difference between tracker1 and ref1
        transform.position = transform.position - new Vector3(trackerPosOffset.x, trackerPosOffset.y, trackerPosOffset.z);

    }


    private void SetCorrectTrackerIndex()
    {
        
        ETrackedPropertyError error = new ETrackedPropertyError();
        for (uint currIndex = 0; currIndex < 16; currIndex++)
        {
            var serial = new System.Text.StringBuilder((int)64);
            var device = new System.Text.StringBuilder((int)64);
            OpenVR.System.GetStringTrackedDeviceProperty(currIndex, ETrackedDeviceProperty.Prop_SerialNumber_String, serial, 64, ref error);
            OpenVR.System.GetStringTrackedDeviceProperty(currIndex, ETrackedDeviceProperty.Prop_ControllerType_String, device, 64, ref error);

            switch (serial.ToString())
            {
                case "LHR-32C6C6BA":
                    tracker1.transform.GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)currIndex);
                    break;
                case "LHR-7A4C6E68":
                    tracker2.transform.GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)currIndex);
                    break;
                case "LHR-55894832":
                    tracker3.transform.GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)currIndex);
                    break;
            }
            Debug.Log("Serial: " + serial + ", Index: " + currIndex.ToString() + ", Device: " + device);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Diagnostics;

public class ControllerScript : MonoBehaviour {

	public OSC osc;

	public GameObject cameraEye;

	public readonly int characterNum = 10; 
	public List<int> availableCharacters;

	public GameObject[,] talkerArr;
	public GameObject talkers;
	private TalkerColors talkerColors;

	private Vector2[] talkerPos;

	private int numTalkers;
	private int talkerDistances;
	public int activeDistance = 0;

	public GameObject clockTextObj;
	private Stopwatch trialClock;
	public bool trialRunning = false;
	public float trialActiveTime; //seconds
    private float remainingTime;


    public List<int> selectedTalkerNum;
	private int[] selectedTalkerNumArr;
  

	// Use this for initialization
	void Start () {


		for (int i = 0; i < characterNum; i++){
			
			availableCharacters.Add (i);
		}
			

		talkerDistances = talkers.transform.childCount;
		numTalkers = 0;

		for (int i=0; i < talkerDistances; i++){

			numTalkers += talkers.transform.GetChild (i).childCount;
		}


		talkerArr = new GameObject[talkerDistances, talkers.transform.GetChild(0).childCount];
		for(int i = 0; i < talkerArr.GetLength(0); i++){
			for (int j = 0; j < talkerArr.GetLength(1); j++) {
				talkerArr [i, j] = talkers.transform.GetChild (i).GetChild(j).gameObject;
			}
		}
			
		trialClock = new Stopwatch ();

		selectedTalkerNumArr = new int[characterNum];

		talkerColors = talkers.GetComponent<TalkerColors> ();

		ResetTalkers ();


	}

	// Update is called once per frame
	void Update () {

        //TIMER
        remainingTime = trialActiveTime - (float)trialClock.Elapsed.TotalSeconds;
        if (remainingTime >= 0.0f && trialRunning)
        {
            clockTextObj.GetComponent<UnityEngine.UI.Text>().text = remainingTime.ToString("F"); // update elapsed time on clock
        }
        else if (remainingTime < 0.0f && trialRunning)
        {

            clockTextObj.GetComponent<UnityEngine.UI.Text>().text = "0.00";
            trialClock.Stop();

        }

	}

	public void ResetTalkers(){


		//reset previous talkers
		for(int i = 0; i < talkerArr.GetLength(0); i++){
			for (int j = 0; j < talkerArr.GetLength (1); j++) {


				talkerArr [i, j].transform.GetComponent<CapsuleCollider> ().enabled = false;

				Material goJointMat = talkerArr [i, j].transform.GetChild (0).GetComponent<Renderer> ().material;
				Material goSurfMat = talkerArr [i, j].transform.GetChild (1).GetComponent<Renderer> ().material;

				goJointMat.SetColor ("_BodyColor", talkerColors.CharToColor (-1));
				goSurfMat.SetColor ("_BodyColor", talkerColors.CharToColor (-1));
				goJointMat.SetColor ("_OutlineColor", talkerColors.blackOutline);
				goSurfMat.SetColor ("_OutlineColor", talkerColors.blackOutline);
				goJointMat.SetFloat ("_BodyAlpha", talkerColors.defaultJointAlpha);
				goSurfMat.SetFloat ("_BodyAlpha", talkerColors.defaultSurfAlpha);



			}
		}

		for (int j = 0; j < talkerArr.GetLength (1); j++) {

			talkerArr [0, j].transform.GetComponent<CapsuleCollider> ().enabled = true;
			activeDistance = 0;

		}

		availableCharacters.Clear ();

		for (int i = 0; i < characterNum; i++){

			availableCharacters.Add (i); // reset available characters list
			selectedTalkerNumArr[i] = -1;
		}

		for(int i = 0; i < talkerArr.GetLength(0); i++){
			for (int j = 0; j < talkerArr.GetLength(1); j++) {

				TalkerController tc = talkerArr [i, j].GetComponent<TalkerController> ();
				tc.character = -1; // reset character of all talkers
				tc.acousticCharacter = -1; // reset character of all acoustic talkers

				Transform aInd = talkerArr [i, j].transform.Find ("AcousticTalkerIndicator");

				if (aInd != null) {
						
					Material aIndMat = aInd.GetComponent<Renderer> ().material;
					aIndMat.SetColor ("_BodyColor", talkerColors.CharToColor (-1));
					aIndMat.SetColor ("_OutlineColor", talkerColors.CharToColor (-1));
					aIndMat.SetFloat ("_BodyAlpha", talkerColors.defaultSurfAlpha);

					aInd.gameObject.SetActive (false);

				}
			}
		}
			
	}


	public void StartNewTrial(){

		trialRunning = true;
		//ResetTalkers ();

		trialClock.Reset ();
		trialClock.Start ();

	}

	public void GetTalkerLabels(){
	
		string selectedTalker = "";
		selectedTalkerNum.Clear ();

		for(int i = 0; i < talkerArr.GetLength(0); i++){
			for (int j = 0; j < talkerArr.GetLength(1); j++) {

				int talkerChar = talkerArr [i, j].GetComponent<TalkerController> ().character;

				if (talkerChar != -1) {
				
					UnityEngine.Debug.Log ("Active talker: " + talkerArr [i, j].name + 
											", Label: " + talkerChar + 
											", Angle: " + (talkerArr [i, j].transform.rotation.eulerAngles.y - 180.0f).ToString("F1") + 
											", Distance: " + Vector3.Distance(gameObject.transform.position, talkerArr [i, j].transform.position).ToString("F1"));

					selectedTalker = selectedTalker + "Talker: " + talkerArr [i, j].name + 
													", Label: " + talkerChar + 
													", Angle: " + (talkerArr [i, j].transform.rotation.eulerAngles.y - 180.0f).ToString("F1") + 
													", Distance: " + Vector3.Distance(gameObject.transform.position, talkerArr [i, j].transform.position).ToString("F1") + "; ";

					selectedTalkerNum.Add((i*talkerArr.GetLength(1) + j) + 1); // dynamic in size
					selectedTalkerNumArr[talkerChar] = (i*talkerArr.GetLength(1) + j) + 1; // stores selected talker at label idx
				}

			}
		}

		//Send azimuth and elevation via OSC
		OscMessage t_message = new OscMessage ();
		t_message.address = "/TalkerResponse";


		for (int i = 0; i < selectedTalkerNumArr.Length; i++) {
			t_message.values.Add (selectedTalkerNumArr[i]);
		}

		osc.Send (t_message);

	}	

	public void ShowAcousticTalker(){

		for (int i = 0; i < talkerArr.GetLength (0); i++) {
			for (int j = 0; j < talkerArr.GetLength (1); j++) {

				int acousticCharacter = talkerArr [i, j].GetComponent<TalkerController> ().acousticCharacter;

				if (acousticCharacter != -1) {
				
					Transform aInd = talkerArr [i, j].transform.Find ("AcousticTalkerIndicator");
					aInd.gameObject.SetActive (true);

					Material aIndMat = aInd.GetComponent<Renderer> ().material;
					aIndMat.SetColor ("_BodyColor", talkerColors.CharToColor (acousticCharacter));
					aIndMat.SetColor ("_OutlineColor", talkerColors.CharToColor (acousticCharacter));
					aIndMat.SetFloat ("_BodyAlpha", talkerColors.selectedSurfAlpha);


				}
			}
		}

	}
}

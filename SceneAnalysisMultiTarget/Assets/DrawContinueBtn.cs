﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawContinueBtn : MonoBehaviour {

	public GameObject continueBtnPrefab;
	public GameObject continueBtn;

	public GameObject continueBtnTxtCanvasPrefab;
	private GameObject continueBtnTxtCanvas;

    private readonly float del = 0.5f;

	public Color defaultColor = new Color (0.0f, 0.7f, 0.0f);
	public Color pressedColor = new Color (0.0f, 1.0f, 0.0f);

	// Use this for initialization
	void Start () {


        StartCoroutine(LateStartRenderModel(del));
        StartCoroutine(LateStart(del + 0.5f));

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator LateStart(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);


        continueBtn = Instantiate (continueBtnPrefab);

        if (UnityEngine.XR.XRDevice.model.Contains("Oculus") && !transform.parent.transform.GetComponent<Valve.VR.SteamVR_TrackedObject>().index.ToString().Equals("None")) {
        
            continueBtn.transform.SetParent(transform.Find("a_button").GetChild(0));
            continueBtn.transform.localPosition = new Vector3(0.0f, 0.00058f, 0.0022f);
        }
        else {

            continueBtn.transform.SetParent(transform.Find("button").GetChild(0));
            continueBtn.transform.localPosition = new Vector3(0.0f, 0.00058f, 0.0f);
        }

        

		Quaternion tmpRot = continueBtn.transform.localRotation;
		tmpRot.eulerAngles = new Vector3 (90.0f, 0.0f, 0.0f);
		continueBtn.transform.localRotation = tmpRot;

		continueBtn.GetComponent<Renderer> ().material.SetColor("_Color", defaultColor);


//		continueBtnTxtCanvas = Instantiate (continueBtnTxtCanvasPrefab);
//		continueBtnTxtCanvas.transform.SetParent(transform.Find ("button"));
//
//		continueBtnTxtCanvas.transform.localPosition = new Vector3(0.0f, 0.00058f, 0.0f);

	}

    IEnumerator LateStartRenderModel(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        transform.GetComponent<Valve.VR.SteamVR_RenderModel>().enabled = true;

    }
}

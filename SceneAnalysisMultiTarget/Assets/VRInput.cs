﻿using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;
using Valve.VR.Extras;

public class VRInput : MonoBehaviour
{

    private ControllerScript cScript;

    public GameObject controller;

    private Valve.VR.Extras.SteamVR_LaserPointer laserPointer;

	public GameObject talkers;
    private TalkerController currGOTalkerContr;
    public GameObject currGO;

    public Material goJointMat;
    public Material goSurfMat;

    private TalkerColors talkerColors;


    private GameObject continueBtn;
    private DrawContinueBtn drawCbtnScript;

	private int charCount;
    public bool canChangeCharacter = false;


    public SteamVR_Behaviour_Pose pose;


    void Start()
	{
		cScript = GetComponent<ControllerScript> ();
		drawCbtnScript = controller.transform.Find ("Model").GetComponent<DrawContinueBtn> ();

    }

    private void OnEnable()
    {

        laserPointer = controller.GetComponent<Valve.VR.Extras.SteamVR_LaserPointer>();

        laserPointer.PointerIn -= HandlePointerIn;
        laserPointer.PointerIn += HandlePointerIn;
        laserPointer.PointerOut -= HandlePointerOut;
        laserPointer.PointerOut += HandlePointerOut;

        if (pose == null)
            pose = controller.GetComponent<SteamVR_Behaviour_Pose>();
        if (pose == null)
            Debug.LogError("No SteamVR_Behaviour_Pose component found on this object");

        talkerColors = talkers.GetComponent<TalkerColors>();

        currGOTalkerContr = new TalkerController();
        currGOTalkerContr.character = -1;
    }


    void Update()
    {
        
        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
        {

            //Debug.Log("Trigger click down!");
        }

        if (SteamVR_Actions._default.GrabPinch.GetStateUp(SteamVR_Input_Sources.Any))
        {


            if (pose != null)
            {

                if (EventSystem.current.currentSelectedGameObject != null)
                {


                    ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler); // The currently clicked gameobject

                    //Debug.Log (currGO.name);

                    //set outline
                    if (EventSystem.current.currentSelectedGameObject.name.Contains("GenericTalker") && cScript.trialRunning)
                    { // if the objected pointed at is a talker

                        currGO = EventSystem.current.currentSelectedGameObject;
                        goJointMat = currGO.transform.GetChild(0).GetComponent<Renderer>().material;
                        goSurfMat = currGO.transform.GetChild(1).GetComponent<Renderer>().material;

                        currGOTalkerContr = currGO.GetComponent<TalkerController>();

                        if (laserPointer.colorIdx == currGOTalkerContr.character)
                        {

                            currGOTalkerContr.character = -1; // reset the character number to default

                            Material goJointMat = currGO.transform.GetChild(0).GetComponent<Renderer>().material;
                            Material goSurfMat = currGO.transform.GetChild(1).GetComponent<Renderer>().material;

                            talkerColors.SetColor(goJointMat, goSurfMat, talkerColors.CharToColor(-1), talkerColors.blackOutline, talkerColors.defaultJointAlpha, talkerColors.defaultSurfAlpha); // set color of talker
                            
                        }
                        else
                        {

                            Color charColor = talkerColors.CharToColor(laserPointer.colorIdx);
                            talkerColors.SetColor(goJointMat, goSurfMat, charColor, charColor, talkerColors.selectedJointAlpha, talkerColors.selectedSurfAlpha); // set color of talker


                            currGOTalkerContr.character = laserPointer.colorIdx;

                            //reset previous talker
                            for (int i = 0; i < cScript.talkerArr.GetLength(0); i++)
                            {
                                for (int j = 0; j < cScript.talkerArr.GetLength(1); j++)
                                {

                                    TalkerController prevTc = cScript.talkerArr[i, j].transform.GetComponent<TalkerController>();

                                    if (prevTc.character == laserPointer.colorIdx && cScript.talkerArr[i, j].name != currGO.name)
                                    {

                                        prevTc.character = -1; // reset character of previous talker

                                        Material PrevGoJointMat = cScript.talkerArr[i, j].transform.GetChild(0).GetComponent<Renderer>().material;
                                        Material PrevGoSurfMat = cScript.talkerArr[i, j].transform.GetChild(1).GetComponent<Renderer>().material;

                                        talkerColors.SetColor(PrevGoJointMat, PrevGoSurfMat, talkerColors.CharToColor(-1), talkerColors.blackOutline, talkerColors.defaultJointAlpha, talkerColors.defaultSurfAlpha); // set color of talker

                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Debug.Log("Trigger click up!");
        }

        if (SteamVR_Actions._default.Squeeze.GetAxis(SteamVR_Input_Sources.Any) > 0.01f)
        {

            //Debug.Log("Trigger press: " + SteamVR_Actions._default.Squeeze.GetAxis(SteamVR_Input_Sources.Any));
        }

        if (SteamVR_Actions._default.TouchPadClick.GetStateDown(SteamVR_Input_Sources.Any))
        {

			//Debug.Log("Touch click down!");
		}

        if (SteamVR_Actions._default.TouchPadClick.GetStateUp(SteamVR_Input_Sources.Any))
        {

            // CHANGE LASER COLOR
            laserPointer.colorIdx = (laserPointer.colorIdx + (int)Mathf.Sign(SteamVR_Actions._default.TouchPadXY.axis.x)) % cScript.characterNum; // set the laser color according to the position of the thumb on the pad

			if (laserPointer.colorIdx < 0) {
                laserPointer.colorIdx = cScript.characterNum + laserPointer.colorIdx;
			}
            laserPointer.color = talkerColors.CharToColor (laserPointer.colorIdx);

            //Debug.Log("Touch click up!");
        }

        if (SteamVR_Actions._default.TouchPadTouch.GetStateDown(SteamVR_Input_Sources.Any))
        {

            //Debug.Log("Touch down!");
        }

        if (SteamVR_Actions._default.TouchPadTouch.GetStateUp(SteamVR_Input_Sources.Any))
        {
            //Debug.Log("Touch up!");
        }

        if (SteamVR_Actions._default.TouchPadXY.axis.x != 0 && SteamVR_Actions._default.TouchPadXY.axis.y != 0 )
        {

            //Debug.Log("Touch xy: " + SteamVR_Actions._default.TouchPadXY.axis.x + ", " + SteamVR_Actions._default.TouchPadXY.axis.y);
        }

        if (SteamVR_Actions._default.InteractUI.GetStateDown(SteamVR_Input_Sources.Any))
        {
			
			if (cScript.trialRunning) { // if one or more talkers are selected
				continueBtn = drawCbtnScript.continueBtn;
				continueBtn.GetComponent<Renderer> ().material.SetColor ("_Color", drawCbtnScript.pressedColor);
			}

            //Debug.Log("UI down!");
        }

        if (SteamVR_Actions._default.InteractUI.GetStateUp(SteamVR_Input_Sources.Any))
        {

			if (cScript.trialRunning) { // if one or more talkers are selected
				continueBtn.GetComponent<Renderer> ().material.SetColor ("_Color", drawCbtnScript.defaultColor);

                cScript.GetTalkerLabels ();
				cScript.trialRunning = false;
			}
			
            //Debug.Log("UI up!");
        }

        if (SteamVR_Actions._default.GrabGrip.GetStateDown(SteamVR_Input_Sources.Any))
        {

            ChangeDistance(); // change the distance of talkers

            //Debug.Log("Grip down!");
        }

        if (SteamVR_Actions._default.GrabGrip.GetStateUp(SteamVR_Input_Sources.Any))
        {

            //Debug.Log("Grip Up!");
        }
    }


    private void HandlePointerIn(object sender, PointerEventArgs e)
    {
        GameObject tmpGO = e.target.gameObject;
        EventSystem.current.SetSelectedGameObject(tmpGO);

        if (EventSystem.current.currentSelectedGameObject.name.Contains("GenericTalker") && cScript.trialRunning)
        {   // if pointing at talker

            currGO = EventSystem.current.currentSelectedGameObject;
            currGOTalkerContr = currGO.GetComponent<TalkerController>();


            goJointMat = currGO.transform.GetChild(0).GetComponent<Renderer>().material;
            goSurfMat = currGO.transform.GetChild(1).GetComponent<Renderer>().material;

            goJointMat.SetFloat("_OutlineWidth", talkerColors.selectedJointOutlineWidth);
            goSurfMat.SetFloat("_OutlineWidth", talkerColors.selectedSurfOutlineWidth);

            if (currGOTalkerContr.character != -1 && cScript.availableCharacters.Count > 0)
            { // if the talker is marked and there are availble characters

                canChangeCharacter = true; // enable option of changing character
            }
            else
            {
                canChangeCharacter = false; // disable option of changing character
            }
        }
    }

    private void HandlePointerOut(object sender, PointerEventArgs e) // if not pointing at talker
    {

        GameObject tmpGO = e.target.gameObject;

        if (tmpGO != null && tmpGO.name.Contains("GenericTalker"))
        {

            goJointMat = tmpGO.transform.GetChild(0).GetComponent<Renderer>().material;
            goSurfMat = tmpGO.transform.GetChild(1).GetComponent<Renderer>().material;

            goJointMat.SetFloat("_OutlineWidth", talkerColors.defaultJointOutlineWidth);
            goSurfMat.SetFloat("_OutlineWidth", talkerColors.defaultSurfOutlineWidth);

        }

        EventSystem.current.SetSelectedGameObject(null);

        currGO = EventSystem.current.currentSelectedGameObject;

        canChangeCharacter = false; // disable option of changing character

    }


    private void ChangeDistance()
    {

        int prevDist = cScript.activeDistance;
        cScript.activeDistance = (prevDist + 1) % cScript.talkerArr.GetLength(0);

        for (int j = 0; j < cScript.talkerArr.GetLength(1); j++)
        {

            cScript.talkerArr[prevDist, j].transform.GetComponent<CapsuleCollider>().enabled = false;
            cScript.talkerArr[cScript.activeDistance, j].transform.GetComponent<CapsuleCollider>().enabled = true;

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Valve.VR.Extras;

public class DrawLaserDot : MonoBehaviour {

	public GameObject laserDotPF;
	private GameObject laserDotInstance;

	private RaycastHit laserPoint;

	public bool laserMaterialChanged = true;

	// Use this for initialization
	void Start () {

		laserDotInstance = Instantiate (laserDotPF);
		laserDotInstance.transform.parent = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {

		if (laserMaterialChanged && laserDotInstance != null) {


			laserDotInstance.transform.GetChild (0).GetComponent<MeshRenderer> ().material = laserDotInstance.transform.parent.Find("New Game Object").GetChild(0).GetComponent<MeshRenderer> ().material;
			laserMaterialChanged = false;
		}

		laserPoint = transform.GetComponent<SteamVR_LaserPointer> ().updatedLaserHitPos;

		try{
			laserDotInstance.transform.position = laserPoint.point;

			Quaternion laserRot = laserDotInstance.transform.rotation;

			laserDotInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, laserPoint.normal);

			Quaternion tmpRot = laserDotInstance.transform.rotation;
			tmpRot.eulerAngles = new Vector3(tmpRot.eulerAngles.x, tmpRot.eulerAngles.y, tmpRot.eulerAngles.z);
			laserDotInstance.transform.rotation = tmpRot;

		}

		#pragma warning disable 0168
		catch(Exception e)
		#pragma warning restore 0168
		{}


	}
}
